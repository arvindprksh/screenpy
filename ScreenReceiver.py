import pickle
import socket
import struct

import cv2


class ScreenReceiver:

    def __init__(self, host, port):
        self.HOST = host
        self.PORT = port

        self.SocketObject = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.SocketObject.bind((self.HOST, self.PORT))
        self.SocketObject.listen(10)
        self.conn, self.address = self.SocketObject.accept()

    def ScreenReceiver(self):
        data = ""
        payload_size = struct.calcsize("L")
        while True:

            while len(data) < payload_size:
                data += self.conn.recv(4096)

            packed_msg_size = data[:payload_size]
            data = data[payload_size:]
            msg_size = struct.unpack("L", packed_msg_size)[0]

            while len(data) < msg_size:
                data += self.conn.recv(4096)
            frame_data = data[:msg_size]
            data = data[msg_size:]

            frame = pickle.loads(frame_data)
            cv2.imshow('frame', frame)
